##### replace
```text
from: "App\Model\Doctrine\Package\Package"
to: "Skadmin\Package\Doctrine\Package\Package"

from: "INSERT INTO package"
to: "INSERT INTO core_package"
```

##### SQL
```sql
INSERT INTO core_package (base_control, webalize, name, is_active) SELECT base_control, webalize, name, is_active FROM package;
DROP TABLE package;
DELETE FROM _nettrine_migrations WHERE version LIKE '%20191002080418';
```