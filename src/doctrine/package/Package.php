<?php

declare(strict_types=1);

namespace Skadmin\Package\Doctrine\Package;

use App\Model\System\ABaseControl;
use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

use function class_exists;

#[ORM\Entity]
#[ORM\Table(name: 'core_package')]
#[ORM\Cache]
#[ORM\HasLifecycleCallbacks]
class Package
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\IsActive;

    #[ORM\Column]
    private string $baseControl = '';

    public function getBaseControl(): ?ABaseControl
    {
        $className = $this->baseControl;
        if (class_exists($className)) {
            return new $className($this->getWebalize());
        }

        return null;
    }
}
