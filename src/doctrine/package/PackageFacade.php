<?php

declare(strict_types=1);

namespace Skadmin\Package\Doctrine\Package;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;

final class PackageFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);

        $this->table = Package::class;
    }

    /**
     * @return Package[]
     */
    public function getAll(): array
    {
        return $this->em
            ->getRepository($this->table)
            ->findAll();
    }

    protected function get(?int $id = null): Package
    {
        $package = parent::get($id);
        assert($package instanceof Package);

        return $package;
    }

    public function getByBaseControl(string $baseControl): ?Package
    {
        $criteria = ['baseControl' => $baseControl];

        $package = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
        assert($package instanceof Package || $package === null);

        return $package;
    }

    public function getByWebalize(string $webalize): ?Package
    {
        $criteria = ['webalize' => $webalize];

        $package = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
        assert($package instanceof Package || $package === null);

        return $package;
    }
}
